package treinamentorestassured;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

public class BuscarPetsStatusPending
{
    @Test
    public void BuscarPetsByStatusPendingTest()
    {
        String[] status = {"pending"};


        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/findByStatus").
                header("content-type", "application/json").
                body(status).
        when().
                get().
        then().
                statusCode(200);
    }
}
