package treinamentorestassured;

import static io.restassured.RestAssured.*;
import org.testng.annotations.Test;
import treinamentorestassured.json.Order;
import static org.hamcrest.Matchers.*;

public class CadastrarPedidoDePet {

    @Test
    public void OrderPetTest()
    {
        Order order = new Order();
        order.setId(1);
        order.setPetId(1);
        order.setQuantity(1);
        order.setShipDate("2022-06-30T15:16:25.661Z");
        order.setStatus("status");
        order.setComplete(true);

        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/store/order").
                header("content-type", "application/json").
                body(order).
        when().
                post().
        then().
                body("petId", equalTo(1),
                     "quantity", equalTo(1),
                     "shipDate", equalTo("2022-06-30T15:16:25.661+0000"),
                     "status", equalTo("status"),
                     "complete", equalTo(true));
    }
}
