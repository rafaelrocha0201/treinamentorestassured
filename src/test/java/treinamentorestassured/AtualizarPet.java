package treinamentorestassured;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;
import treinamentorestassured.json.Category;
import treinamentorestassured.json.PetJson;
import treinamentorestassured.json.Tag;

public class AtualizarPet {

    @Test
    public void AtualizarPetTest()
    {
        PetJson petjson = new PetJson();
        petjson.setId("201");
        Category category = new Category();
        category.setId(1);
        category.setName("Categoria");
        petjson.setCategory(category);
        petjson.setName("Rex");
        petjson.setPhotoUrls(new String[]{"http://fotosdegato.com.br/foto1.png", "http://fotosdegato.com.br/foto2.png"});
        Tag tag1 = new Tag();
        tag1.setID(1);
        tag1.setName("tag1");
        petjson.setTags(new Tag[]{tag1});
        petjson.setStatus("available");

        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet").
                header("content-type", "application/json").
                body(petjson).
        when().
                put().
        then().
                body("id", equalTo(201),
                        "category.id", equalTo(1),
                        "category.name", equalTo("Categoria"),
                        "name", equalTo("Rex"),
                        "photoUrls[0]", equalTo("http://fotosdegato.com.br/foto1.png"),
                        "photoUrls[1]", equalTo("http://fotosdegato.com.br/foto2.png"),
                        "tags[0].id", equalTo(1),
                        "tags[0].name", equalTo("tag1"),
                        "status", equalTo("available"));
    }
}
