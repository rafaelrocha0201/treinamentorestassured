package treinamentorestassured;
import static io.restassured.RestAssured.*;
import org.testng.annotations.Test;

public class PesquisarPetInexistente {

    @Test
    public void PesquisarPetInexistenteTest()
    {
        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet/{petId}").
                pathParam("petId", 25).
                header("content-type", "application/json").
        when().
                get().
        then().
                statusCode(404);
    }
}
