package treinamentorestassured.json;


public class PetJson {
    private String id;
    Category category;
    private String name;
    private String[] photoUrls;
    private Tag[] tags;
    private String status;


    // Getter Methods

    public String getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String[] getPhotoUrls() {
        return photoUrls;
    }

    public Tag[] getTags() {
        return tags;
    }

    public String getStatus() {
        return status;
    }

    // Setter Methods

    public void setId(String id) {
        this.id = id;
    }


    public void setCategory(Category category) {
        this.category = category;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhotoUrls(String[] value) {
        this.photoUrls = value;
    }

    public void setTags(Tag[] value) {
        this.tags = value;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
