package treinamentorestassured.json;

public class Tag {
    private long id;
    private String name;

    public long getID() {
        return id;
    }

    public String getName() {
        return name;
    }


    public void setID(long value) {
        this.id = value;
    }

    public void setName(String value) {
        this.name = value;
    }
}

