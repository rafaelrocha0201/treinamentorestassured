package treinamentorestassured;

import org.testng.annotations.Test;
import treinamentorestassured.json.Category;
import treinamentorestassured.json.PetJson;
import treinamentorestassured.json.Tag;

import static io.restassured.RestAssured.*;

public class CadastrarPetComMetodoErrado {

    @Test
    public void BuscarPetComMetodoErradoTest()
    {
        PetJson petjson = new PetJson();
        petjson.setId("teste");
        Category category = new Category();
        category.setId(1);
        category.setName("Categoria");
        petjson.setCategory(category);
        petjson.setName("Rex");
        petjson.setPhotoUrls(new String[]{"http://fotosdegato.com.br/foto1.png", "http://fotosdegato.com.br/foto2.png"});
        Tag tag1 = new Tag();
        tag1.setID(1);
        tag1.setName("tag1");
        petjson.setTags(new Tag[]{tag1});
        petjson.setStatus("available");

        given().
                baseUri("https://petstore.swagger.io/v2").
                basePath("/pet").
                header("content-type", "application/json").
                body(petjson).
        when().
                get().
        then().
                statusCode(405);
    }
}
